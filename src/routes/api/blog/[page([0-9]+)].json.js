import getPosts from '../../../getPosts.js';

export function get(req, res) {
	let { page } = req.params;
	const contents = new getPosts().get();

	res.writeHead(200, {
		'Content-Type': 'application/json'
	});

	res.end(JSON.stringify(contents));
}