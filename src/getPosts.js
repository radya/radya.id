import posts from './_posts.js';
import config from './config.js';

class getPosts {
	constructor() {
		this.posts = posts
	}

	page(n) {
		--n;
		this.posts = posts.slice(n * config.perPage, (n + 1) * config.perPage);
	}

	get() {
		return this.posts;
	}
}

export default getPosts;